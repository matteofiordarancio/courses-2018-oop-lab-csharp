﻿using System;

namespace Properties {

    public class Card
    {

        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }
        
        public string Seed
        {
            get => seed;
        }
        
        public string Name
        {
            get => name;
        }
        
        public int Ordinal
        {
            get => ordial;
        }

        public override string ToString()
        {
            return $"{this.GetType().Name}(Name={this.Name}, Seed={this.Seed}, Ordinal={this.Ordinal})";
        }
        
    }

}