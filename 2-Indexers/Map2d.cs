﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {

        private Dictionary<Tuple<TKey1,TKey2>, TValue> map = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            return map.Equals(other);
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get { return map[new Tuple<TKey1, TKey2>(key1, key2)]; }
            set { map.Remove(new Tuple<TKey1, TKey2>(key1, key2)); map.Add(new Tuple<TKey1, TKey2>(key1, key2), value); }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> temp = new List<Tuple<TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> t in map.Keys)
            {
                if(t.Item1.Equals(key1))
                {
                    temp.Add(new Tuple<TKey2, TValue>(t.Item2, map[t]));
                }
            }
            return temp;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> temp = new List<Tuple<TKey1, TValue>>();
            foreach (Tuple<TKey1, TKey2> t in map.Keys)
            {
                if (t.Item2.Equals(key2))
                {
                    temp.Add(new Tuple<TKey1, TValue>(t.Item1, map[t]));
                }
            }
            return temp;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> temp = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach(Tuple<TKey1,TKey2> t in map.Keys)
            {
                temp.Add(new Tuple<TKey1, TKey2, TValue>(t.Item1, t.Item2, map[t]));
            }
            return temp;
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            foreach( TKey1 i in keys1)
            {
                foreach ( TKey2 j in keys2)
                {
                    map.Add(new Tuple<TKey1, TKey2>(i,j), generator(i,j));
                }
            }
        }

        public int NumberOfElements
        {
            get
            {
                return map.Count;
            }
        }

        public override string ToString()
        {
            string stringa = "";
            foreach(KeyValuePair<Tuple<TKey1, TKey2>, TValue> temp in map)
            {
                stringa += ("\n" + temp + "\n");
            }
            return stringa;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
